/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sqliteproject1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (1,'KimNamJoon','rm136' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (2,'KimSeokJin','ksj136' );";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (3,'minyoongi','myg136' );";

            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (4,'JungHoSeok','jhs136' );";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (5,'ParkJiMin','pjm136' );";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (6,'KimTaeHyung','kth136' );";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (7,'JeonJungKook','jjk136' );";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
